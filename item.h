#pragma once
#include <iostream>
using namespace std;
class Item
{
string name;
int weight, price;

public:

	Item(string name, int weight, int price) 
	{
		this->name = name;
		this->weight = weight;
		this->price = price;
	}

	string getName() 
	{
		return name;
	}

	int getWeight() 
	{
		return weight;
	}

	int getPrice() 
	{
		return price;
	}

};