#include <iostream>
#include "generator.h"
#include <fstream>
#include "item.h"
using namespace std;




int main()
{
	setlocale(LC_ALL, "Russian");

	Generator myGenerator;
	vector<vector<int>> allComb;
	vector<int> bestComb_1;
	vector<int> bestComb_2;
	vector<int> bestComb_3;
	vector<vector<int>> matrix_of_comb;
	myGenerator.readFile("input.txt");

	// 1 �����
	allComb = myGenerator.Algorithm(0, 0, 0);// ������� ��� ����������
	myGenerator.PrintCombinations(allComb); //������

	bestComb_1 = myGenerator.GetBestCombination();// ������ ����������
	myGenerator.PrintAllBestCombination();//������� ������ ����������(����� ���� ���������)

	// 2 �����
	matrix_of_comb = myGenerator.Dynamic_1();// ������� ��������\����
	myGenerator.PrintMatrix(matrix_of_comb);// ������

	// �������������� �����(�� ����� ��������� ���������� ������ ����)
	bestComb_2 = myGenerator.Recovery_Ans_1(matrix_of_comb.size()-1, matrix_of_comb[0].size()-1);
	myGenerator.PrintCombination(bestComb_2);//������

	// ������������� �������
	matrix_of_comb = myGenerator.Dynamic_2(2);// ������� ��������\����
	myGenerator.PrintMatrix(matrix_of_comb);// ������

	bestComb_3 = myGenerator.Recovery_Ans_2(matrix_of_comb.size() - 1, matrix_of_comb[0].size() - 1);
	myGenerator.PrintCombination(bestComb_3);//������

	//cout << endl << "Price(recursion) = " << myGenerator.PriceCombinations(bestComb_1);
	//cout << endl;
	//cout << endl << "Price(table) = " << myGenerator.Matrix;
	
	return 0;
}