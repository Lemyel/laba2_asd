﻿#pragma once
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include "item.h"
#include <algorithm>


using namespace std;

class Generator 
{  
    vector<Item> items;
    // лучшая комбинация рюкзака
    vector<vector<int>> bestCombinations;
    // вектор хранящий все варианты составленного рюкзака
    vector<vector<int>> allCombinations;
    // вектор хранящий составленный рюкзак в опредленный момент
    vector<int> сurrentCombinations;

    // матрица в которой предметы берутся 0 или 1 раз
    vector<vector<int>> MatrixCombinations_1;
    // матрица в которой предметы берутся от 0 до k раз
    vector<vector<int>> MatrixCombination_2;
    // восстановленный ответ 
    vector<int> ans_1;
    vector<int> ans_2;
    // сколько раз можно брать предмет
    int count = 1;

    int N = 0, W = 0, i = 0;
 public:
    void readFile(string name) 
    {
        fstream input(name);
        string line;
        if (input.is_open())
        {
            // Общее число предметов
            getline(input, line);
            N = stoi(line.c_str());
            // Максимальный вес рюкзака
            getline(input, line);
            W = stoi(line.c_str());

            bestCombinations.resize(1);
            bestCombinations[0].resize(N, 0);
            сurrentCombinations.resize(N, 0);
            ans_1.resize(N, 0);
            ans_2.resize(N, 0);
            MatrixCombinations_1.resize((int)(N + 1));
            for (int i = 0; i < MatrixCombinations_1.size(); i++)
            {
                MatrixCombinations_1[i].resize((int)(W + 1), 0);
            }
            MatrixCombination_2.resize((int)(N + 1));
            for (int i = 0; i < MatrixCombination_2.size(); i++)
            {
                MatrixCombination_2[i].resize((int)(W + 1), 0);
            }
            cout << "Общее число предметов = "<<N << endl 
                << "Максимальный вес рюкзака = " << W << endl;

            for (int i = 0; i < N; i++)
            {
                string name;
                int weight, price;

                input >> name;
                input >> weight;
                input >> price;

                items.push_back(Item(name, weight, price)); 

            }
            for (int i = 0; i < items.size(); i++)
            {
                cout << items[i].getName() << " ";
                cout << items[i].getWeight() << " ";
                cout << items[i].getPrice()<<endl;
            }
        }
        else
        {
            cout << "Файл не найден!" << endl;
        }
    }
    int WeightCombinations(vector<int> CombinationsItems)
    {
        int w = 0;
        for (int i = 0; i < CombinationsItems.size(); i++)
        {
            w += CombinationsItems[i] * items[i].getWeight();
        }
        return w;
    }
    int PriceCombinations(vector<int> CombinationsItems)
    {
        int p = 0;
        for (int i = 0; i < CombinationsItems.size(); i++)
        {
            p += CombinationsItems[i] * items[i].getPrice();
        }
        return p;
    }
    vector<vector<int>> Algorithm(int startIdx, int cur_w, int cur_p)
    {
        if(startIdx == сurrentCombinations.size())
        {
            if (cur_p >= PriceCombinations(bestCombinations.back()))
            {
                //bestCombinationsItems = сurrentCombinationsItems; 
                bestCombinations.push_back(сurrentCombinations);
            }   
           allCombinations.push_back( сurrentCombinations);
        }
        else
        {
            for (int j = 0;  j < 2; j++)
            {
                if (cur_w + j * items[startIdx].getWeight() <= W)
                {
                    сurrentCombinations[startIdx] = j;
                    Algorithm(startIdx + 1, cur_w + j * items[startIdx].getWeight(),
                        cur_p + j * items[startIdx].getPrice());
                }
            }     
        }
        return allCombinations;
    }
    vector<int> GetBestCombination()
    {
        return bestCombinations.back();
    }
    vector<vector<int>> Dynamic_1()
    {       
        for (int i = 1; i <= N; i++)
        {
            for (int j = 1; j <= W; j++) 
            {          
                int cur_w = items[i - 1].getWeight();
                int cur_p = items[i - 1].getPrice();
                if (j >= cur_w)
                {
                    MatrixCombinations_1[i][j] = max(MatrixCombinations_1[i - 1][j],
                        MatrixCombinations_1[i - 1][j - cur_w] + cur_p);
                }
                else
                {
                    MatrixCombinations_1[i][j] = MatrixCombinations_1[i - 1][j];
                }
            }
        }
        return MatrixCombinations_1;    
    }

    vector<int> Recovery_Ans_1(int k, int s)
    {
        if (MatrixCombinations_1[k][s] == 0) return ans_1;
        if (MatrixCombinations_1[k - 1][s] == MatrixCombinations_1[k][s])
        {
            Recovery_Ans_1(k - 1, s);
        }
        else
        {
            Recovery_Ans_1( k - 1, s - items[k - 1].getWeight());
            ans_1[k - 1] = 1;
        }  
        return ans_1;
    }
    vector<vector<int>> Dynamic_2(int k)
    {
        count = k;
        for (int i = 1; i <= items.size(); i++)
        {
            for (int j = 0; j <= W; j++)
            {
                int cur_w = items[i - 1].getWeight();
                int cur_p = items[i - 1].getPrice();
                MatrixCombination_2[i][j] = MatrixCombination_2[i - 1][j];
                for (int l = 1; l <= min(k, j / cur_w); l++)
                {
                    MatrixCombination_2[i][j] = max(MatrixCombination_2[i][j], MatrixCombination_2[i - 1][j - l * cur_w] + l * cur_p);
                }
            }
        }
        return MatrixCombination_2;
    }
    vector<int> Recovery_Ans_2(int k, int s)
    {
        if (MatrixCombination_2[k][s] == 0) return ans_2;
        if (MatrixCombination_2[k - 1][s] == MatrixCombination_2[k][s])
        {
            Recovery_Ans_2(k - 1, s);  
        }
        else
        {
            for (int l = 1; l <= min(count, s / items[k - 1].getWeight()); l++)
            {
                if (MatrixCombination_2[k][s] == 
                    MatrixCombination_2[k - 1][s - l * items[k - 1].getWeight()] + l * items[k - 1].getPrice())
                {
                    Recovery_Ans_2(k - 1, s - l * items[k - 1].getWeight());
                    ans_2[k - 1] = l;
                }               
            }  
        }   
        return ans_2;
    }
    // печать
    void PrintCombinations(vector<vector<int>> combinations)
    {
        for (int i = 0; i < combinations.size(); i++)
        {
            for (int j = 0; j < combinations[i].size(); j++)
            {
                cout << items[j].getName() << " " << combinations[i][j] << " | ";     
            }
            cout << WeightCombinations(combinations[i]) << "\t| " << PriceCombinations(combinations[i]);
            cout << endl;       
        }
    }
    void PrintAllBestCombination()
    {
        cout << "Best Combinations:" << endl;
        for (int i = 0; i < allCombinations.size(); i++)
        {
            if (PriceCombinations(allCombinations[i]) ==
                PriceCombinations(bestCombinations.back()))
            {
                PrintCombination(allCombinations[i]);
            }
        }
        /* int max = PriceCombinations(bestCombinationsItems[bestCombinationsItems.size() - 2]);
         for (int i = 0; i < n; i++)
         {
             while (max == PriceCombinations(bestCombinationsItems.back()))
             {
                 Print(bestCombinationsItems.back());
                 int max = PriceCombinations(bestCombinationsItems[bestCombinationsItems.size() - 3]);
                 bestCombinationsItems.pop_back();
             }
             Print(bestCombinationsItems.back());
             int max = PriceCombinations(bestCombinationsItems[bestCombinationsItems.size() - 3]);
             bestCombinationsItems.pop_back();
         }*/
    }
    void PrintCombination(vector<int> combinations)
    {
        for (int i = 0; i < combinations.size(); i++)
        {
            cout << items[i].getName() << " " << combinations[i] << " | ";
        }
        cout << WeightCombinations(combinations) << "\t| " << PriceCombinations(combinations);
        cout << endl;
    }
    void PrintMatrix(vector<vector<int>> M)
    {
        cout << endl << "TABLE: " << endl;
        for (int i = 0; i <= W; i++)
        {
            if (i == 0)
                cout << "    " << i << "   | ";
            else
                cout << i << "\t| ";
        }
        cout << endl;
        for (int i = 0; i <= W * 9; i++)
            cout << "=";
        cout << endl;
        for (int i = 0; i < M.size(); i++)
        {
            cout << i << "|| ";
            for (int j = 0; j < M[i].size(); j++)
            {
                cout << M[i][j] << "\t| ";
            }
            cout << endl;
        }

    }
    void Print()
    {
        for (int i = 0; i < ans_1.size(); i++)
        {         
                cout << ans_1[i] << " | ";      
        }
    }
};
