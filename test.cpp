#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "Generator.h"
#include <vector>

using namespace std;


TEST_CASE("Check vector elements", "[vector]") {
	setlocale(LC_ALL, "Russian");
	setlocale(LC_ALL, "Russian");

	Generator myGenerator;
	vector<vector<int>> allComb;
	vector<int> bestComb_1;
	vector<int> bestComb_2;
	vector<int> bestComb_3;
	int count = 1;
	myGenerator.readFile("input.txt");


	allComb = myGenerator.Algorithm(0, 0, 0);
	bestComb_1 = myGenerator.GetBestCombination();

	myGenerator.Dynamic_1();
	bestComb_2 = myGenerator.Recovery_Ans_1(myGenerator.
		Dynamic_1().size() - 1, myGenerator.Dynamic_1()[0].size() - 1);

	myGenerator.Dynamic_2(1);
	bestComb_3 = myGenerator.Recovery_Ans_2(myGenerator.
		Dynamic_2(count).size() - 1, myGenerator.Dynamic_2(count)[0].size() - 1);

	REQUIRE(myGenerator.PriceCombinations(bestComb_1) 
		== myGenerator.PriceCombinations(bestComb_2));

	REQUIRE(myGenerator.PriceCombinations(bestComb_2)
		== myGenerator.PriceCombinations(bestComb_3));

	cout << endl << "1 algorithm" << endl;
	myGenerator.PrintCombination(bestComb_1);
	cout << endl << "2 algorithm" << endl;
	myGenerator.PrintCombination(bestComb_2);
	cout << endl << "3 algorithm(individual task) " << endl;
	myGenerator.PrintCombination(bestComb_2);
	




}
